package ru.t1.karimov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.karimov.tm.api.repository.ICommandRepository;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.command.data.AbstractDataCommand;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.repository.CommandRepository;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;
import ru.t1.karimov.tm.service.*;
import ru.t1.karimov.tm.util.SystemUtil;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.karimov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);


    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@Nullable final String arg) throws AbstractException {
        @Nullable final AbstractCommand abstractArgument = commandService.getCommandByArgument(arg);
        if (abstractArgument == null) throw new ArgumentNotSupportedException(arg);
        abstractArgument.execute();
    }

    public void processCommand(@Nullable final String command) throws AbstractException {
        processCommand(command, true);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initBackup() throws AbstractException {
        backup.init();
    }

    private void initDemoData() throws AbstractException {
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP));
        if (checkBase64) return;

        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin","admin", Role.ADMIN);

        final String user1 = userService.findByLogin("test").getId();
        final String user2 = userService.findByLogin("user").getId();
        final String user3 = userService.findByLogin("admin").getId();

        projectService.add(new Project(user1,"Project_01", "Desc_01_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1, "Project_02", "Desc_02_user 1", Status.NOT_STARTED));
        projectService.add(new Project(user1, "Project_03", "Desc_03_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1, "Project_04", "Desc_04_user 1", Status.COMPLETED));
        projectService.add(new Project(user2,"Project_01", "Desc_01_user 2", Status.IN_PROGRESS));
        projectService.add(new Project(user2, "Project_02", "Desc_02_user 2", Status.NOT_STARTED));
        projectService.add(new Project(user3, "Project_01", "Desc_01_user 3", Status.IN_PROGRESS));
        projectService.add(new Project(user3, "Project_02", "Desc_02_user 3", Status.COMPLETED));

        final String projectId1 = projectService.findOneByIndex(user1,3).getId();
        final String projectId2 = projectService.findOneByIndex(user2,1).getId();
        final String projectId3 = projectService.findOneByIndex(user3,1).getId();

        taskService.add(new Task(user1, "Task_01", "Desc task 1 user 1", Status.IN_PROGRESS, null));
        taskService.add(new Task(user1, "Task_02", "Desc task 2 user 1", Status.NOT_STARTED, null));
        taskService.add(new Task(user1, "Task_03", "Desc task 3 user 1", Status.COMPLETED, projectId1));
        taskService.add(new Task(user1, "Task_04", "Desc task 4 user 1", Status.NOT_STARTED, projectId1));
        taskService.add(new Task(user2, "Task_01", "Desc task 1 user 2", Status.IN_PROGRESS, projectId2));
        taskService.add(new Task(user2, "Task_02", "Desc task 2 user 2", Status.NOT_STARTED, null));
        taskService.add(new Task(user3, "Task_01", "Desc task 1 user 3", Status.COMPLETED, projectId3));
        taskService.add(new Task(user3, "Task_01", "Desc task 2 user 3", Status.NOT_STARTED, null));
    }

    private void initLogger() {
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")
        ));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    void processCommand(@Nullable final String command, final boolean checkRoles) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void start(@Nullable final String[] args) throws AbstractException {
        processArguments(args);
        initPID();
        initLogger();
        initDemoData();
        initBackup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
