package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    void login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws AbstractException;

    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role[] roles) throws AbstractException;

}
