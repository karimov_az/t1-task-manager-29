package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return records.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return records.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return records.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return records.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
